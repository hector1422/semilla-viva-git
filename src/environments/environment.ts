// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCfQyWZk4_mTljcq6s3CmDHOonIl4I3FjY",
    authDomain: "semilla-viva.firebaseapp.com",
    databaseURL: "https://semilla-viva.firebaseio.com",
    projectId: "semilla-viva",
    storageBucket: "semilla-viva.appspot.com",
    messagingSenderId: "376336215442",
    appId: "1:376336215442:web:40128ba50c0c938384a54b",
    measurementId: "G-50BNKRFWBY"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
