export const environment = {
  production: true,
  firebaseConfig : {
    apiKey: "AIzaSyCfQyWZk4_mTljcq6s3CmDHOonIl4I3FjY",
    authDomain: "semilla-viva.firebaseapp.com",
    databaseURL: "https://semilla-viva.firebaseio.com",
    projectId: "semilla-viva",
    storageBucket: "semilla-viva.appspot.com",
    messagingSenderId: "376336215442",
    appId: "1:376336215442:web:40128ba50c0c938384a54b",
    measurementId: "G-50BNKRFWBY"
  }
};
