import { SimpleChanges } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { Contacto } from './Model/contacto.model';
import { AuthService } from './Modules/admin/Services/auth.service';
import { ElementsService } from './Services/elements.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'semilla-viva';
  faCoffee = faCoffee;
  contacto: Contacto;
  log: boolean;
  tipo: string;
  in = false; qs = false; not = false; prod = false; ev = false; ppad = false; 
  art = false; cont = false; sem = false; car = false; hor = false;otr = false;
  gran = false; adm = false; fru = false; l = false;
  constructor(private router: Router, private elementService: ElementsService,
    private authService: AuthService, private route: ActivatedRoute,
    private location: Location) {
    this.contacto = new Contacto();
    this.log = false;
  }
  ngOnInit(): void {

    this.getContacto();
    this.log = this.authService.isLoggedIn;

  }
  ngDoCheck(): void {

    this.tipo = this.router.url;
    this.log = this.authService.isLoggedIn;
    switch (this.tipo) {
      case '/home':
        this.reiniciarBanderas();
        this.in = true;
        break;
      case '/quienes-somos':
        this.reiniciarBanderas();
        this.qs = true;
        break;
      case '/noticias':
        this.reiniciarBanderas();
        this.not = true;
        break;
      case '/productores':
        this.reiniciarBanderas();
        this.prod = true;
        break;
      case '/eventos':
        this.reiniciarBanderas();
        this.ev = true;
        break;
      case '/padres':
        this.reiniciarBanderas();
        this.ppad = true;
        break;
      case '/articulos':
        this.reiniciarBanderas();
        this.art = true;
        break;
      case '/contactanos':
        this.reiniciarBanderas();
        this.cont = true;
        break;
      case '/admin':
        this.reiniciarBanderas();
        this.adm = true;
        break;
      case '/semilla':
        this.reiniciarBanderas();
        this.sem = true;
        break;
      case '/grano':
        this.reiniciarBanderas();
        this.gran = true;
        break;
      case '/carne':
        this.reiniciarBanderas();
        this.car = true;
        break;
      case '/hortalizas':
        this.reiniciarBanderas();
        this.hor = true;
        break;  
      case '/otros':
        this.reiniciarBanderas();
        this.otr = true;
        break;
      case '/frutas':
        this.reiniciarBanderas();
        this.fru = true;
        break;
      default:

        break;
    }

  }

  getContacto() {
    this.elementService.getContacto().subscribe(result => {
      result.forEach(c => {
        let cont = new Contacto();
        cont.celular = c.celular;
        cont.correo = c.correo;
        cont.direccion = c.direccion;
        cont.facebook = c.facebook;
        cont.twitter = c.twitter;
        cont.instagram = c.instagram;
        this.contacto = cont;
      }
      );

    });
  }
  selIn() {
    this.reiniciarBanderas();
    this.in = true;
  }
  selL() {
    this.reiniciarBanderas();
    this.l = true;
  }
  selAdm() {
    this.reiniciarBanderas();
    this.adm = true;
  }
  selQs() {
    this.reiniciarBanderas();
    this.qs = true;
  }
  selNot() {
    this.reiniciarBanderas();
    this.not = true;
  }
  selProd() {
    this.reiniciarBanderas();
    this.prod = true;
  }
  selEv() {
    this.reiniciarBanderas();
    this.ev = true;
  }
  selPpad() {
    this.reiniciarBanderas();
    this.ppad = true;
  }
  selArt() {
    this.reiniciarBanderas();
    this.art = true;
  }
  selCont() {
    this.reiniciarBanderas();
    this.cont = true;
  }
  selSem() {
    this.reiniciarBanderas();
    this.sem = true;
  }
  selGran(){
    this.reiniciarBanderas();
    this.gran = true;
  }
  selCar(){
    this.reiniciarBanderas();
    this.car = true;
  }
  selHor(){
    this.reiniciarBanderas();
    this.hor = true;
  }
  selOtr(){
    this.reiniciarBanderas();
    this.otr = true;
  }
  selFru(){
    this.reiniciarBanderas();
    this.fru = true;
  }
  reiniciarBanderas() {
    this.in = false;
    this.qs = false;
    this.not = false;
    this.prod = false;
    this.ev = false;
    this.ppad = false;
    this.art = false;
    this.cont = false;
    this.adm = false;
    this.l = false;
    this.sem = false;
    this.gran = false;
    this.car = false;
    this.hor = false;
    this.otr = false;
    this.fru = false;
  }
  quienesSomos() {
    this.selQs();
    this.router.navigate(['/quienes-somos']);
  }
  login() {
    this.selL();
    this.router.navigate(['/login']);
  }
  noticias() {
    this.selNot();
    this.router.navigate(['/noticias']);
  }
  // pediatras() {
  //   this.selPed();
  //   this.router.navigate(['/pediatras']);
  // }
  productores() {
    this.selProd();
    this.router.navigate(['/productores']);
  }
  eventos() {
    this.selEv();
    this.router.navigate(['/eventos']);
  }
  home() {
    this.selIn();
    this.router.navigate(['/home']);
  }
  admin() {
    this.selAdm();
    this.router.navigate(['/admin']);
  }
  logout() {
    this.selAdm();
    this.authService.SignOut();
    this.router.navigate(['/admin']);
  }
  paraPadres() {
    this.selPpad();
    this.router.navigate(['/padres']);
  }
  articulos() {
    this.selArt();
    this.router.navigate(['/articulos']);
  }
  contactanos() {
    this.selCont();
    this.router.navigate(['/contactanos']);
  }
  semillas() {
    this.selSem();
    this.router.navigate(['/semillas']);
  }
  granos(){
    this.selGran();
    this.router.navigate(['/granos']);
  }
  carnes(){
    this.selCar();
    this.router.navigate(['/carnes']);
  }
  hortalizas(){
    this.selHor();
    this.router.navigate(['/hortalizas']);
  }
  otros(){
    this.selOtr();
    this.router.navigate(['/otros']);
  }
  frutas(){
    this.selFru();
    this.router.navigate(['/frutas']);
  }
}

