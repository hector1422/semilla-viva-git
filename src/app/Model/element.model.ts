import {  firestore } from 'firebase/app';
import Timestamp = firestore.Timestamp;
export class Element {
    documento:string;
    productor:string;
    telefono_productor:string;
    hora_atencion:string;
    ubicacion_productor:string;
    tipo:string;
    imagen:string;
    titulo:string;
    tipo_cultivo:string;
    resumen:string;
    descripcion_larga:string;
    fecha:Timestamp;
    constructor()
    {

    }
  }