import { ElementsAdminService } from './Services/elements-admin.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutes } from './admin.routing';
import { AdminFunctionsComponent } from './Components/admin-functions/admin-functions.component';
import { ListNoticiasAdminComponent } from './Components/admin-functions/list-noticias-admin/list-noticias-admin.component'
import { EditarElementoAdminComponent } from './Components/admin-functions/editar-elemento-admin/editar-elemento-admin.component'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MyMatPaginatorInt } from '../admin/Components/traduccion/my-mat-paginator-int';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {AddElementoAdminComponent} from './Components/admin-functions/add-elemento-admin/add-elemento-admin.component'
import {EditarQuienesSomosComponent} from './Components/admin-functions/editar-quienes-somos/editar-quienes-somos.component'
import {EditarCamposAdminComponent} from './Components/admin-functions/editarCampos-admin/editarCampos-admin.component'
import {ListPediatrasAdminComponent} from './Components/admin-functions/list-pediatras-admin/list-pediatras-admin.component'
import {AgregarPediatraAdminComponent} from './Components/admin-functions/list-pediatras-admin/agregar-pediatra-admin/agregar-pediatra-admin.component'
import {EditarPediatraAdminComponent} from './Components/admin-functions/list-pediatras-admin/editar-pediatra-admin/editar-pediatra-admin.component'
import {VerPediatraAdminComponent} from './Components/admin-functions/list-pediatras-admin/ver-pediatra-admin/ver-pediatra-admin.component'
import {VerNoticiaAdminComponent} from './Components/admin-functions/ver-noticia-admin/ver-noticia-admin.component'
import {LoginAdminComponent} from './Components/login-admin/login-admin.component';
@NgModule({
  declarations: [
    AdminFunctionsComponent,
    ListNoticiasAdminComponent,
    EditarElementoAdminComponent,
    AddElementoAdminComponent,
    EditarQuienesSomosComponent,
    EditarCamposAdminComponent,
    ListPediatrasAdminComponent,
    AgregarPediatraAdminComponent,
    EditarPediatraAdminComponent,
    VerPediatraAdminComponent,
    VerNoticiaAdminComponent,
    LoginAdminComponent
   
  ],
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatStepperModule,
    MatSelectModule,
    MatNativeDateModule,
    MatCardModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFirestoreModule,
    AngularFireModule,
    AngularFireAuthModule
  ],
  providers: [ElementsAdminService,
    { provide: MatPaginatorIntl, useClass: MyMatPaginatorInt },
    { provide: MatDialogRef, useValue: {} }]

})
export class AdminModule { }
