import { ElementsService } from 'src/app/Services/elements.service';
import { Element } from '../../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import Timestamp = firestore.Timestamp;
import { firestore } from 'firebase';
@Component({
  selector: 'app-editar-elemento-admin',
  templateUrl: './editar-elemento-admin.component.html',
  styleUrls: ['./editar-elemento-admin.component.scss']
})
export class EditarElementoAdminComponent implements OnInit {
  noticia: Element;
  formulario: FormGroup;
  nuevaNoticia: Element;
  tipos: string[] = ['Semillas', 'Granos', 'Carnes', 'Hortalizas', 'Frutas', 'Otros'];
  tipos_cultivos: string[] = ['Agroecológico', 'Orgánico', 'Convencional', 'No aplica'];
  image: any;
  imageBandera = false;
  source:string='';
  constructor(public dialogoReg: MatDialogRef<EditarElementoAdminComponent>, private formBuilder: FormBuilder, private elementService: ElementsService) {
    this.noticia = new Element(); //debe ser any para la KEY
    this.nuevaNoticia = new Element();
  }
  ngOnInit() {
    this.imageBandera = false;
    this.crearFormulario();
  }
  onSubmit() {
    this.nuevaNoticia = new Element();
    this.nuevaNoticia.titulo = this.formulario.get('titulo').value;
    this.nuevaNoticia.tipo = this.formulario.get('tipo').value;
    this.nuevaNoticia.tipo_cultivo = this.formulario.get('tipo_cultivo').value;
    this.nuevaNoticia.resumen = this.formulario.get('resumen').value;
    this.nuevaNoticia.fecha = this.formulario.get('fecha').value;
    this.nuevaNoticia.descripcion_larga = this.formulario.get('descripcion_larga').value;
    if (this.imageBandera) {
      this.elementService.editElementImage(this.image, this.noticia, this.nuevaNoticia);
    }

    else {
      this.nuevaNoticia.imagen = this.noticia.imagen;
      this.nuevaNoticia.fecha = Timestamp.now();
      this.elementService.editElement(this.noticia, this.nuevaNoticia);
    }
    this.okMessage();
    this.dialogoReg.close();
  }
  crearFormulario() {
    this.formulario = this.formBuilder.group(
      {
        tipo: [this.noticia.tipo, [Validators.required]],
        tipo_cultivo: [this.noticia.tipo_cultivo, [Validators.required]],
        titulo: [this.noticia.titulo, [Validators.required]],
        resumen: [this.noticia.resumen, [Validators.required]],
        descripcion_larga: [this.noticia.descripcion_larga, [Validators.required]],
        fecha: [this.noticia.fecha, [Validators.required]],
        imagen: [this.noticia.imagen, [Validators.required]],
      });
    this.formulario.valueChanges.subscribe(
      value => {
        /*  console.log("Otro valor Contacto: ", value); */
      }
    );
  }
  errores(): boolean 
  {
    return (this.formulario.get('titulo').hasError('required') ||
      this.formulario.get('tipo').hasError('required') ||
      this.formulario.get('tipo_cultivo').hasError('required') ||
      this.formulario.get('resumen').hasError('required') ||
      this.formulario.get('descripcion_larga').hasError('required')  ||
      this.formulario.get('fecha').hasError('required')
      ) ? true : false;

  }
  handleImage(event: any): void {
    this.image = event.target.files[0];
    this.imageBandera = true;
    this.projectImage(event.target.files[0]);
  }
  
  projectImage(file: File) {
    let reader = new FileReader;
    // TODO: Define type of 'e'
    reader.onload = (e: any) => {
        // Simply set e.target.result as our <img> src in the layout
        this.source = e.target.result;
        
    };
    // This will process our file and get it's attributes/data
    reader.readAsDataURL(file);
}
  okMessage() {
    Swal.fire({
      icon: 'success',
      title: 'Producto Editado',
      text: 'El producto fué editado Correctamente!',
    });
    this.ngOnInit();
  }
  cancel() {
    Swal.fire({
      icon: 'error',
      title: 'Producto NO Editado',
      text: 'El producto NO fué editado!',
    });
    this.dialogoReg.close();
  }

}
