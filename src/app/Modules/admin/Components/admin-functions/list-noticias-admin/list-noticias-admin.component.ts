import { EditarElementoAdminComponent } from '../editar-elemento-admin/editar-elemento-admin.component';
import { ElementsAdminService } from '../../../Services/elements-admin.service';
import { Element } from '../../../../../Model/element.model';
import { Component, OnInit ,ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { VerNoticiaAdminComponent } from '../ver-noticia-admin/ver-noticia-admin.component';
@Component({
  selector: 'app-list-noticias-admin',
  templateUrl: './list-noticias-admin.component.html',
  styleUrls: ['./list-noticias-admin.component.scss']
})
export class ListNoticiasAdminComponent implements OnInit {
  // displayedColumns: string[] = ['tipo', 'titulo', 'tipo_cultivo','fecha', 'opciones'];
  displayedColumns: string[] = ['tipo', 'titulo', 'tipo_cultivo', 'opciones'];
  dataSource: MatTableDataSource<Element>;
  vacio:boolean;
  noticias: Element[];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  
  constructor(private elementService: ElementsAdminService, private dialog: MatDialog) {
    this.noticias = [];
  }

  ngOnInit() {
    this.vacio=true;
    
    this.noticias = [];
    this.dataSource = new MatTableDataSource(this.noticias);
    this.dataSource.paginator = this.paginator;
    this.obtenerElementos();
      }
  obtenerElementos() {
    
    this.elementService.getElementos().subscribe(result => {this.noticias=[];  
      result.forEach(e => {
        this.noticias.push(e);
        // this.noticias.sort(this.a);
      }
      );
      this.dataSource = new MatTableDataSource(this.noticias);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    if(this.noticias.length>0) this.vacio=false;
     
  }
  // a(a,b){   
  //   return b.fecha.seconds-a.fecha.seconds;
  // }
  verElemento(n: Element) {
    const dialogRef = this.dialog.open(VerNoticiaAdminComponent, {
      width: '800px',
      height: '500px',
      data: {}
    });
    dialogRef.componentInstance.noticia = n;
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  editarElemento(n: any) {
    const dialogRef = this.dialog.open(EditarElementoAdminComponent, {
      width: '800px',
      height: '500px',
      data: {}
    });
    dialogRef.componentInstance.noticia = n;
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }
  eliminar(row:Element)
  {  
    Swal.fire({
    title: 'Está seguro?',
    text: "El elemento será eliminado!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Borralo!'
  }).then((result) => {
    if (result.isConfirmed) {
      this.elementService.elimElement(row);
      this.ngOnInit();         
      Swal.fire(
        'Borrado!',
        'El elemento ha sido borrado.',
        'success'
      );
    }
    else
    {
      Swal.fire(
        'Cancelado!','',
        'error'
      )
    }
  })
  }
  okMessage()
  {
    
    Swal.fire({
      icon: 'success',
      title: 'Elemento Eliminado',
      text: 'El elemento fué eliminado Correctamente!',
    });
  }

}
