import { QuienesSomos } from '../../../../../Model/quienes-somos.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ElementsService } from 'src/app/Services/elements.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-quienes-somos',
  templateUrl: './editar-quienes-somos.component.html',
  styleUrls: ['./editar-quienes-somos.component.scss']
})
export class EditarQuienesSomosComponent implements OnInit {
  formulario:FormGroup;
  quienesSomos:QuienesSomos;
  quienesSomosRef:any;
  constructor(private elementService:ElementsService, private formBuilder: FormBuilder) 
  { 
    this.quienesSomos=new QuienesSomos();
  }

  ngOnInit() {
    /* this.addQuienesSomos(); */
    this.crearFormulario();
    this.obtenerQuienesSomos();
  }
/*   addQuienesSomos()
  {
    let cont= new QuienesSomos();
            cont.historia="La Sociedad Colombiana de Pediatría (SCP) fue fundada el 27 de julio de 1917 con el nombre de Sociedad Pediátrica de Bogotá, cuya Personería Jurídica correspondió a la No. 338 del 18 de julio de 1919. Mediante Resolución Ejecutiva No. 54 del 12 de mayo de 1944, emanada de la Presidencia de la República, se le reconoció la Personería con nueva denominación de Sociedad Colombiana de Pediatría. Esta Personería Jurídica fue renovada con el No. 941 del 11 de Marzo de 1975, según Resolución de la Oficina Jurídica del Ministerio de Justicia. La SCP es la representante de la Pediatría Colombiana ante las instituciones nacionales y extranjeras. La Federación Colombiana de Pediatría fue fundada el 29 de julio de 1970 y obtuvo la Personería Jurídica No. 5387, mediante Resolución del mismo número del 4 de julio de 1979, emanada del Ministerio de Salud.";
            cont.mision="Propender al bienestar de los niños, niñas y adolescentes, su familia y la sociedad; así como el progreso de sus asociados, el fortalecimiento de los vínculos entre sí y con el resto del mundo, para el desarrollo de la pediatría como disciplina del hombre.";
            cont.vision="Ser la asociación que represente ante el Estado colombiano e internacionalmente a todos los pediatras y subespecialistas de la pediatría acreditados en el territorio nacional.";  
    this.elementService.addQuienesSomos(cont);  
  } */
  obtenerQuienesSomos()
  {
    this.elementService.getQuienesSomos().subscribe(result=>{     
      result.forEach(c=>
        {
            this.quienesSomosRef=c;
            let cont= new QuienesSomos();
            cont.historia=c.historia;
            cont.mision=c.mision;
            cont.vision=c.vision;            
            this.quienesSomos=cont;
            this.formulario.controls['historia'].setValue(cont.historia);
            this.formulario.controls['mision'].setValue(cont.mision);
            this.formulario.controls['vision'].setValue(cont.vision);
        });        
      });
  }
  crearFormulario() {
    this.formulario = this.formBuilder.group(
      {
        historia: [this.quienesSomos.historia, [Validators.required]],
        mision: [this.quienesSomos.mision, [Validators.required]],
        vision: [this.quienesSomos.vision, [Validators.required]]
      });
    this.formulario.valueChanges.subscribe(
      value => {
        console.log("Otro valor Quienes Somos: ",value);
      }
    );
  }
  onSubmit()
  {
     let ct=new QuienesSomos();
    ct.historia=this.formulario.get('historia').value;
    
    ct.mision=this.formulario.get('mision').value;
    
    ct.vision=this.formulario.get('vision').value;
    console.log("### ELEMENTO A GUARDAR:   ",ct); 
    this.elementService.editQuienesSomos(this.quienesSomosRef,ct);
    this.okMessage();
  }
  okMessage()
  {
    Swal.fire({
      icon: 'success',
      title: 'Contacto Editado',
      text: 'El contacto fué editado Correctamente!',
    });
    this.ngOnInit()
  }

}
