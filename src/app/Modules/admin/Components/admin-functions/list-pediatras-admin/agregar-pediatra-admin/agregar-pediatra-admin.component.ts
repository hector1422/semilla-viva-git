import { PediatraAdminService } from '../../../../Services/pediatra-admin.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Pediatra } from 'src/app/Model/pediatra.model';
import Swal from 'sweetalert2';
import {ErrorStateMatcher} from '@angular/material/core';
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-agregar-pediatra-admin',
  templateUrl: './agregar-pediatra-admin.component.html',
  styleUrls: ['./agregar-pediatra-admin.component.scss']
})
export class AgregarPediatraAdminComponent implements OnInit {
  formulario: FormGroup;
  image: any;
  pediatra:Pediatra;
  imageBandera:boolean;
  source:string='';
  constructor(public dialogoReg:MatDialogRef<AgregarPediatraAdminComponent>,
    private _formBuilder: FormBuilder, private pediatraService: PediatraAdminService) 
    { 
      this.imageBandera = false;

    }

  ngOnInit() {
    this.pediatra=new Pediatra();
    this.crearFormulario();
  }
  crearFormulario() {
    this.formulario = this._formBuilder.group({
      cedula: ['', [Validators.required,Validators.pattern('^[0-9]*$')]],
      nombre: ['', [Validators.required,Validators.pattern('^[A-Za-z-ñÑáéíóúÁÉÍÓÚ ]+[a-zA-Z\\s]*')]],
      contrasenia: ['', [Validators.required]],  
      telefono: ['', [Validators.required,Validators.pattern('^[0-9]*$')]],
      hora_atencion: ['', [Validators.required]],     
      restriccion: ['', [Validators.required]], 
      ubicacion: ['', [Validators.required]]
    });
  }
  openInput() {
    document.getElementById("fileInput").click();
  }
  onUpload() {
    this.pediatra=new Pediatra();
    this.pediatra.cedula=this.formulario.get('cedula').value;
    this.pediatra.nombre=this.formulario.get('nombre').value;
    this.pediatra.contrasenia=this.formulario.get('contrasenia').value;
    this.pediatra.telefono=this.formulario.get('telefono').value;
    this.pediatra.hora_atencion=this.formulario.get('hora_atencion').value;
    this.pediatra.restriccion=this.formulario.get('restriccion').value;
    this.pediatra.ubicacion=this.formulario.get('ubicacion').value;
    this.pediatra.imagen="";    
    this.pediatraService.uploadPediatra(this.image, this.pediatra);
    this.okMessage();
  }
  handleImage(event: any): void {
    this.image = event.target.files[0];
    this.imageBandera = true;
    this.projectImage(event.target.files[0]);
  }
  errores(): boolean {
    return (this.formulario.get('cedula').hasError('required') ||
    this.formulario.get('cedula').hasError('pattern') ||
    this.formulario.get('nombre').hasError('required') ||
    this.formulario.get('nombre').hasError('pattern') ||
    this.formulario.get('contrasenia').hasError('required') ||
    this.formulario.get('telefono').hasError('required') ||
    this.formulario.get('telefono').hasError('pattern') ||
    this.formulario.get('hora_atencion').hasError('required') ||
    this.formulario.get('restriccion').hasError('required') ||
    this.formulario.get('ubicacion').hasError('required') || !this.imageBandera
      ) ? true : false;

  }
  okMessage()
  {
    Swal.fire({
      icon: 'success',
      title: 'Elemento publicado',
      text: 'El elemento ha sido publicado!',
    });
    this.ngOnInit();
    this.dialogoReg.close();
  }
  projectImage(file: File) {
    let reader = new FileReader;
    // TODO: Define type of 'e'
    reader.onload = (e: any) => {
        // Simply set e.target.result as our <img> src in the layout
        this.source = e.target.result;
        
    };
    // This will process our file and get it's attributes/data
    reader.readAsDataURL(file);
}
  cancel()
  {
    Swal.fire({
      icon: 'error',
      title: 'Productor no Agregado',
      text: 'El productor no ha sido agregado!',
    });
    this.dialogoReg.close();
  }


}
