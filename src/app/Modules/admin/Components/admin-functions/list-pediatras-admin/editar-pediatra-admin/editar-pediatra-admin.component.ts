import { PediatraAdminService } from '../../../../Services/pediatra-admin.service';
import { Component, OnInit } from '@angular/core';
import { Pediatra } from '../../../../../../Model/pediatra.model';

import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-editar-pediatra-admin',
  templateUrl: './editar-pediatra-admin.component.html',
  styleUrls: ['./editar-pediatra-admin.component.scss']
})
export class EditarPediatraAdminComponent implements OnInit {
  pediatra:Pediatra;
  pediatraRef:any;
  nuevoPediatra:Pediatra;
  formulario: FormGroup;
  image: any;
  imageBandera:boolean;
  source:string='';
  constructor(public dialogoReg: MatDialogRef<EditarPediatraAdminComponent>, 
    private formBuilder: FormBuilder, private pediatraService:PediatraAdminService) { }

  ngOnInit() {
    this.imageBandera = false;
    this.crearFormulario();
  }
  crearFormulario() {
    this.formulario = this.formBuilder.group(
      {
        nombre: [this.pediatraRef.nombre, [Validators.required,Validators.pattern('^[A-Za-z-ñÑáéíóúÁÉÍÓÚ ]+[a-zA-Z\\s]*')]],
        cedula: [this.pediatraRef.cedula, [Validators.required,Validators.pattern('^[0-9]*$')]],
        telefono: [this.pediatraRef.telefono, [Validators.required,Validators.pattern('^[0-9]*$')]],
        hora_atencion: [this.pediatraRef.hora_atencion, [Validators.required]],       
        restriccion: [this.pediatraRef.restriccion, [Validators.required]],
        ubicacion: [this.pediatraRef.ubicacion, [Validators.required]],
        contrasenia: [this.pediatraRef.contrasenia, [Validators.required]]      
      });
    this.formulario.valueChanges.subscribe(
      value => {
        /*  console.log("Otro valor Contacto: ", value); */
      }
    );
  }
  onSubmit() {
    this.nuevoPediatra = new Pediatra();
    this.nuevoPediatra.nombre = this.formulario.get('nombre').value;
    this.nuevoPediatra.cedula = this.formulario.get('cedula').value;
    this.nuevoPediatra.contrasenia = this.formulario.get('contrasenia').value;
    this.nuevoPediatra.telefono = this.formulario.get('telefono').value;
    this.nuevoPediatra.hora_atencion = this.formulario.get('hora_atencion').value;
    this.nuevoPediatra.restriccion = this.formulario.get('restriccion').value;
    this.nuevoPediatra.ubicacion = this.formulario.get('ubicacion').value;

    if (this.imageBandera) {
      this.pediatraService.editElementImage(this.image, this.pediatraRef, this.nuevoPediatra);
    }

    else {
      this.nuevoPediatra.imagen = this.pediatraRef.imagen;
      this.pediatraService.editPediatra(this.pediatraRef, this.nuevoPediatra);
    }
    this.okMessage();
    this.dialogoReg.close();
  }
  handleImage(event: any): void {
    this.image = event.target.files[0];
    this.imageBandera = true;
    this.projectImage(event.target.files[0]);
  }
  errores(): boolean {
    return (this.formulario.get('nombre').hasError('required') ||
      this.formulario.get('nombre').hasError('pattern') ||
      this.formulario.get('contrasenia').hasError('require') ||
      this.formulario.get('cedula').hasError('required') ||
      this.formulario.get('cedula').hasError('pattern') ||
      this.formulario.get('telefono').hasError('required') ||
      this.formulario.get('telefono').hasError('pattern') ||
      this.formulario.get("hora_atencion").hasError('required') ||
      this.formulario.get("restriccion").hasError('required') ||
      this.formulario.get("ubicacion").hasError('required')
      ) ? true : false;

  }
  projectImage(file: File) {
    let reader = new FileReader;
    // TODO: Define type of 'e'
    reader.onload = (e: any) => {
        // Simply set e.target.result as our <img> src in the layout
        this.source = e.target.result;
        
    };
    // This will process our file and get it's attributes/data
    reader.readAsDataURL(file);
}
okMessage() {
  Swal.fire({
    icon: 'success',
    title: 'Productor Editado',
    text: 'El productor fué editado Correctamente!',
  });
  this.ngOnInit();
}
cancel() {
  Swal.fire({
    icon: 'error',
    title: 'Productor NO Editado',
    text: 'El productor NO fué editado!',
  });
  this.dialogoReg.close();
}

}
