import { EditarPediatraAdminComponent } from './editar-pediatra-admin/editar-pediatra-admin.component';
import { Pediatra } from '../../../../../Model/pediatra.model';
import { PediatraAdminService } from '../../../Services/pediatra-admin.service';
import { Component, OnInit,ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { VerPediatraAdminComponent } from './ver-pediatra-admin/ver-pediatra-admin.component';
import { AgregarPediatraAdminComponent } from './agregar-pediatra-admin/agregar-pediatra-admin.component';
@Component({
  selector: 'app-list-pediatras-admin',
  templateUrl: './list-pediatras-admin.component.html',
  styleUrls: ['./list-pediatras-admin.component.scss']
})
export class ListPediatrasAdminComponent implements OnInit {
  displayedColumns: string[] = ['nombre', 'cedula', 'telefono', 'opciones'];
  dataSource: MatTableDataSource<Pediatra>;
  vacio:boolean;
  pediatras: Pediatra[];
  productor: string[];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  
  constructor(private pediatraService: PediatraAdminService, private dialog: MatDialog) 
  { 
    this.pediatras=[];
  }

  ngOnInit() {
    // this.pediatras=[];
    this.dataSource = new MatTableDataSource(this.pediatras);
    this.dataSource.paginator = this.paginator;
    this.obtenerElementos();
  }
  agregar()
  {
    const dialogRef = this.dialog.open(AgregarPediatraAdminComponent, {
      width: '800px',
      height: '500px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  obtenerElementos() {
    
    this.pediatraService.getPediatras().subscribe(result => {    this.pediatras=[];  
      result.forEach(e => {
        this.pediatras.push(e);
      }
      );
      this.dataSource = new MatTableDataSource(this.pediatras);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
   
  }
  verPediatra(p: Pediatra) {
    const dialogRef = this.dialog.open(VerPediatraAdminComponent, {
      width: '800px',
      height: '500px',
      data: {}
    });
    dialogRef.componentInstance.p = p;
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  editarPediatra(n: any) {
    const dialogRef = this.dialog.open(EditarPediatraAdminComponent, {
      width: '800px',
      height: '500px',
      data: {}
    });
    dialogRef.componentInstance.pediatraRef = n;
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }
  eliminar(row:Pediatra)
  {  
    Swal.fire({
    title: 'Está seguro?',
    text: "El Productor será eliminado del registro!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Borralo!'
  }).then((result) => {
    if (result.isConfirmed) {
      this.pediatraService.elimPediatra(row);
      this.ngOnInit();         
      Swal.fire(
        'Borrado!',
        'El productor se ha eliminado del Registro.',
        'success'
      );
    }
    else
    {
      Swal.fire(
        'Cancelado!','Productor No Eliminado del Registro',
        'error'
      )
    }
  })
  }
  okMessage()
  {
    
    Swal.fire({
      icon: 'success',
      title: 'Elemento Eliminado',
      text: 'El elemento fué eliminado Correctamente!',
    });
  }

}
