import { Injectable } from '@angular/core';
import { Pediatra } from '../../../Model/pediatra.model';
import {AngularFirestore, AngularFirestoreCollection,} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
const COLECCION_PEDIATRAS: string = '/productores';

@Injectable({
  providedIn: 'root'
})
export class PediatraAdminService {
  private downloadURL: Observable<string>;
  private pediatraDB: AngularFireList<Pediatra>;
  private filePath: any;
constructor( private afs: AngularFirestore,
  private storage: AngularFireStorage,
  private db: AngularFireDatabase) 
  { 
    this.pediatraDB = this.db.list(COLECCION_PEDIATRAS, (ref) =>
      ref.orderByChild('nombre')
    );
  }
  elimPediatra(pediatraRef:any) {
    this.db.object(`${COLECCION_PEDIATRAS}/${pediatraRef.$key}`).remove();
  }
  addPediatra(pediatra: Pediatra) {
    return this.pediatraDB.push(pediatra);
  }
  uploadPediatra(image: any, pediatra:Pediatra) {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((urlImage) => {
            this.downloadURL = urlImage;
            pediatra.imagen = urlImage;
            this.addPediatra(pediatra);
          });
        })
      )
      .subscribe();
  }
  getPediatras(): Observable<Pediatra[]> {
    return this.pediatraDB.snapshotChanges().pipe(
      map((changes) => {
        return changes.map((c) => ({
          $key: c.payload.key,
          ...c.payload.val(),
        }));
      })
    );
  }
  editPediatra(pediatraRef:any,pediatra:Pediatra) {
    this.db.object(`${COLECCION_PEDIATRAS}/${pediatraRef.$key}`)
    .update({ cedula: pediatra.cedula, nombre: pediatra.nombre, contrasenia: pediatra.contrasenia, telefono: pediatra.telefono, hora_atencion: pediatra.hora_atencion, restriccion: pediatra.restriccion, ubicacion: pediatra.ubicacion, imagen: pediatra.imagen});
  }
  editElementImage(image:any, pediatraRef:any,pediatra:Pediatra)
  {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((urlImage) => {
            this.downloadURL = urlImage;
            pediatra.imagen = urlImage;
            this.editPediatra(pediatraRef, pediatra);
          });
        })
      )
      .subscribe();
  }

}
