import { Injectable } from '@angular/core';
import { QuienesSomos } from '../../../Model/quienes-somos.model';
import { Contacto } from '../../../Model/contacto.model';
import { Element } from '../../../Model/element.model';
import {AngularFirestore, AngularFirestoreCollection,} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';
import { FileI } from '../../../Model/file.model';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { element } from 'protractor';

const COLECCION_ELEMENTOS: string = '/elementos';
const COLECCION_CONTACTO: string = '/contacto';
const COLECCION_QUIENES_SOMOS: string = '/quienes-somos';
@Injectable()
export class ElementsAdminService {

  private postsCollection: AngularFirestoreCollection<Element>;
  private filePath: any;
  private downloadURL: Observable<string>;
  private elementDB: AngularFireList<Element>;
  private contactDB: AngularFireList<Contacto>;  
  private quienesSomosDB: AngularFireList<QuienesSomos>;
  public evento: Element;
  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage,
    private db: AngularFireDatabase
  ) {
    this.elementDB = this.db.list(COLECCION_ELEMENTOS, (ref) =>
      ref.orderByChild('fecha')
    );
    this.contactDB = this.db.list(COLECCION_CONTACTO, (ref) =>
      ref.orderByChild('correo')
    );
    this.quienesSomosDB = this.db.list(COLECCION_QUIENES_SOMOS, (ref) =>
    ref.orderByChild('historia')
  );
   
  }

  addContacto(contacto: Contacto) {
    return this.contactDB.push(contacto);
  }
  getContacto(): Observable<Contacto[]> {
    return this.contactDB.snapshotChanges().pipe(
      map((changes) => {
        return changes.map((c) => ({
          $key: c.payload.key,
          ...c.payload.val(),
        }));
      })
    );
  }
  editContact(contactoRef:any,cont:Contacto) {
    this.db.object(`${COLECCION_CONTACTO}/${contactoRef.$key}`)
    .update({ celular: cont.celular, correo: cont.correo, 
      direccion: cont.direccion, facebook: cont.facebook, twitter: cont.twitter, instagram: cont.instagram });
  }
  //SECCIÓN QUIENES SOMOS #########################################
  addQuienesSomos(qs: QuienesSomos) {
    return this.quienesSomosDB.push(qs);
  }
  getQuienesSomos(): Observable<QuienesSomos[]> {
    return this.quienesSomosDB.snapshotChanges().pipe(
      map((changes) => {
        return changes.map((c) => ({
          $key: c.payload.key,
          ...c.payload.val(),
        }));
      })
    );
  }
  editQuienesSomos(quienesSomosRef:any,qs:QuienesSomos) {
    this.db.object(`${COLECCION_QUIENES_SOMOS}/${quienesSomosRef.$key}`)
    .update({ historia: qs.historia, mision: qs.mision, vision: qs.vision});
  }
  //##########################################################################

  addElemento(elemento: Element) {   
    if(elemento != null){
      let productos:Element = {
        "documento":elemento["productor"]["cedula"],
        "productor":elemento["productor"]["nombre"],
        "telefono_productor":elemento["productor"]["telefono"],
        "hora_atencion":elemento["productor"]["hora_atencion"],
        "ubicacion_productor":elemento["productor"]["ubicacion"],
        "descripcion_larga": elemento["descripcion_larga"],
        "fecha": elemento["fecha"],
        "imagen": elemento["imagen"],
        "resumen": elemento["resumen"],
        "tipo": elemento["tipo"],
        "tipo_cultivo": elemento["tipo_cultivo"],
        "titulo": elemento["titulo"]
      }
      // console.log("RESULTADO: ",productos);
      return this.elementDB.push(productos);
    }
   
  }
  uploadElement(image: any, element: Element) {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((urlImage) => {
            this.downloadURL = urlImage;
            element.imagen = urlImage;
            this.addElemento(element);
          });
        })
      )
      .subscribe();
  }
  editElement(elementoRef:any,elem:Element) {
    this.db.object(`${COLECCION_ELEMENTOS}/${elementoRef.$key}`)
    .update({ titulo: elem.titulo, 
      tipo: elem.tipo, 
      tipo_cultivo: elem.tipo_cultivo, 
      productor: elem.productor, 
      resumen: elem.resumen, 
      imagen: elem.imagen, 
      fecha: elem.fecha, 
      descripcion_larga: elem.descripcion_larga});
  }
  elimElement(elementoRef:any) {
    this.db.object(`${COLECCION_ELEMENTOS}/${elementoRef.$key}`).remove();
  }
  editElementImage(image:any, elementoRef:any,elem:Element)
  {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((urlImage) => {
            this.downloadURL = urlImage;
            elem.imagen = urlImage;
            this.editElement(elementoRef, elem);
          });
        })
      )
      .subscribe();
  }
  getEvent(): Element {
    return this.evento;
  }
 
  getElementos(): Observable<Element[]> {
    return this.elementDB.snapshotChanges().pipe(      
      map((changes) => {
        return changes.map((c) => ({
          $key: c.payload.key,
          ...c.payload.val(),
        }));
      })
    );
  }
  cambiarEvento(evento: Element) {
    this.evento = evento;
  }

}
