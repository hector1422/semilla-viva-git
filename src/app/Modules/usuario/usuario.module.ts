import { HomeComponent } from './Components/home/home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {VerPediatraUsuarioComponent} from './Components/ver-pediatra-usuario/ver-pediatra-usuario.component';
import {ListPediatrasComponent} from './Components/list-pediatras/list-pediatras.component'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {ListNoticiasComponent} from './Components/list-noticias/list-noticias.component'
import {VerNoticiaComponent} from './Components/ver-noticia/ver-noticia.component' 
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {ContactoComponent} from './Components/contacto/contacto.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ListArticulosComponent} from './Components/list-articulos/list-articulos.component'
import {ListParaPadresComponent} from './Components/list-para-padres/list-para-padres.component'
import {ListEventosComponent} from './Components/list-eventos/list-eventos.component'
import {QuienesSomosComponent} from './Components/quienes-somos/quienes-somos.component';
import {CategoriasUsuarioComponent} from './Components/categorias-usuario/categorias-usuario.component'
import {MatExpansionModule} from '@angular/material/expansion';
import {HttpClientModule} from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import {UbicacionUsuarioComponent} from './Components/ubicacion-usuario/ubicacion-usuario.component'
import {YouTubePlayerModule} from '@angular/youtube-player';
import { SemillasComponent } from './Components/semillas/semillas.component';
import { VerSemillaComponent } from './Components/ver-semilla/ver-semilla.component';
import { VerGranoComponent } from './Components/ver-grano/ver-grano.component';
import { CategoriasGranoComponent } from './Components/categorias-grano/categorias-grano.component';
import { GranosComponent } from './Components/granos/granos.component';
import { CategoriasCarneComponent } from './Components/categorias-carne/categorias-carne.component';
import { CarnesComponent } from './Components/carnes/carnes.component';
import { VerCarneComponent } from './Components/ver-carne/ver-carne.component';
import { CategoriasHortalizaComponent } from './Components/categorias-hortaliza/categorias-hortaliza.component';
import { VerHortalizaComponent } from './Components/ver-hortaliza/ver-hortaliza.component';
import { HortalizasComponent } from './Components/hortalizas/hortalizas.component';
import { CategoriasOtroComponent } from './Components/categorias-otro/categorias-otro.component';
import { OtrosComponent } from './Components/otros/otros.component';
import { VerOtroComponent } from './Components/ver-otro/ver-otro.component';
import { ListProductoresComponent } from './Components/list-productores/list-productores.component';
import { FrutasComponent } from './Components/frutas/frutas.component';
import { CategoriasFrutasComponent } from './Components/categorias-frutas/categorias-frutas.component';
import { VerFrutaComponent } from './Components/ver-fruta/ver-fruta.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@NgModule({
  
  declarations: [VerPediatraUsuarioComponent,
    ListPediatrasComponent,
    ListNoticiasComponent,
    VerNoticiaComponent,
    HomeComponent,
    ContactoComponent,
    ListArticulosComponent,
    QuienesSomosComponent,
    ListParaPadresComponent,
    ListEventosComponent,
    CategoriasUsuarioComponent,
    UbicacionUsuarioComponent,
    SemillasComponent,
    VerSemillaComponent,
    CategoriasGranoComponent,
    GranosComponent,
    VerGranoComponent,
    CategoriasCarneComponent,
    CarnesComponent,
    VerCarneComponent,
    CategoriasHortalizaComponent,
    HortalizasComponent,
    VerHortalizaComponent,
    CategoriasOtroComponent,
    OtrosComponent,
    VerOtroComponent,
    ListProductoresComponent,
    CategoriasFrutasComponent,
    FrutasComponent,
    VerFrutaComponent


  ],
    


    imports: [
    CommonModule,
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatStepperModule,
    MatSelectModule,
    MatNativeDateModule,
    MatCardModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    HttpClientModule,
    YouTubePlayerModule,
    Ng2SearchPipeModule,
    AgmCoreModule.forRoot({
         apiKey: 'AIzaSyCfQyWZk4_mTljcq6s3CmDHOonIl4I3FjY'
      // apiKey: 'AIzaSyDVHlGp6mCOotZiOkevL9O9Rma8tRdLQBU'  
   }),
  ],
  providers: [
    { provide: MatDialogRef, useValue: {} }]
})
export class UsuarioModule { }
