import { QuienesSomosComponent } from './Components/quienes-somos/quienes-somos.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { ListNoticiasComponent } from './Components/list-noticias/list-noticias.component';
import { ListArticulosComponent } from './Components/list-articulos/list-articulos.component';
import { ContactoComponent } from './Components/contacto/contacto.component';
import { SemillasComponent } from './Components/semillas/semillas.component';
import { GranosComponent } from './Components/granos/granos.component';
import { CarnesComponent } from './Components/carnes/carnes.component';
import { HortalizasComponent } from './Components/hortalizas/hortalizas.component';
import { OtrosComponent } from './Components/otros/otros.component';
import { ListProductoresComponent } from './Components/list-productores/list-productores.component';
import { FrutasComponent } from './Components/frutas/frutas.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },{
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'quienes-somos',
    component: QuienesSomosComponent,
  },
  // {
  //   path: 'pediatras',
  //   component: ListPediatrasComponent,
  // },
  {
    path: 'productores',
    component: ListProductoresComponent,
  },
  {
    path: 'noticias',
    component: ListNoticiasComponent,
   
  },
  {
    path: 'eventos',
    component: ListArticulosComponent,
   
  },
  {
    path: 'padres',
    component: ListNoticiasComponent,
   
  },
  {
    path: 'articulos',
    component: ListArticulosComponent,
   
  },
  {
    path: 'contactanos',
    component: ContactoComponent,
    
  },
  {
    path: 'semillas',
    component: SemillasComponent,
  },
  {
    path: 'granos',
    component: GranosComponent,
  },
  {
    path: 'carnes',
    component: CarnesComponent,
  },
  {
    path: 'hortalizas',
    component: HortalizasComponent,
  },
  {
    path: 'otros',
    component: OtrosComponent,
  },
  {
    path: 'frutas',
    component: FrutasComponent,
  }
];

export const UsuarioRoutes = RouterModule.forChild(routes);
