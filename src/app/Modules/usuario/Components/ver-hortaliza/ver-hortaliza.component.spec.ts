/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { VerHortalizaComponent } from './ver-hortaliza.component';

describe('VerHortalizaComponent', () => {
  let component: VerHortalizaComponent;
  let fixture: ComponentFixture<VerHortalizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerHortalizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerHortalizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
