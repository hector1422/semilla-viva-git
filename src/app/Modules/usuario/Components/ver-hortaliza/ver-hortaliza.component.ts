import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-ver-hortaliza',
  templateUrl: './ver-hortaliza.component.html',
  styleUrls: ['./ver-hortaliza.component.scss']
})
export class VerHortalizaComponent implements OnInit {
  hortaliza:Element;
  constructor(public dialogoReg:MatDialogRef<VerHortalizaComponent>) 
  { 
    this.hortaliza=new Element();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }

}
