import { Component, OnInit } from '@angular/core';
import { Pediatra } from 'src/app/Model/pediatra.model';
import { PediatrasService } from 'src/app/Services/pediatras.service';
import { MatDialog } from '@angular/material/dialog';
import { VerPediatraUsuarioComponent } from '../ver-pediatra-usuario/ver-pediatra-usuario.component';
import { Element } from 'src/app/Model/element.model';
@Component({
  selector: 'app-list-productores',
  templateUrl: './list-productores.component.html',
  styleUrls: ['./list-productores.component.scss']
})
export class ListProductoresComponent implements OnInit {
  pediatras: Pediatra[];

  searchedKeyword: string;
  constructor(private pediatraService: PediatrasService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.pediatras=[];
    this.obtenerElementos();
  }
  obtenerElementos() {
    
    this.pediatraService.getPediatras().subscribe(result => {this.pediatras=[];  
      result.forEach(e => {
        this.pediatras.push(e);
      }
      );      
    });
   
  }
  verPediatra(p: Pediatra, e:Element) {
    const dialogRef = this.dialog.open(VerPediatraUsuarioComponent, {
      width: '800px',
      height: '500px',
      data: {}
    });
    dialogRef.componentInstance.p = p;
    // dialogRef.componentInstance.e = e;
    // console.log(`Dialog result: ${e.descripcion_larga}`);
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
}
