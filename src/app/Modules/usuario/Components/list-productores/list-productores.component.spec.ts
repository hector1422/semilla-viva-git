import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProductoresComponent } from './list-productores.component';

describe('ListProductoresComponent', () => {
  let component: ListProductoresComponent;
  let fixture: ComponentFixture<ListProductoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProductoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProductoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
