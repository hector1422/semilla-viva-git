import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { VerCarneComponent } from '../ver-carne/ver-carne.component';
const NUMERO_CARNES_RECIENTES=3;
@Component({
  selector: 'app-categorias-carne',
  templateUrl: './categorias-carne.component.html',
  styleUrls: ['./categorias-carne.component.scss']
})
export class CategoriasCarneComponent implements OnInit {
  panelOpenState = false;
  carnes:Element[];
  @Input() tipo:string;
  constructor(private elementService:ElementUsuarioService, private router: Router,
    private dialog: MatDialog) 
  { 
    this.carnes= [];
    this.tipo="Carnes";
  }

  ngOnInit(): void {
    this.obtenerElementos();
    this.tipo=this.router.url;
    if(this.tipo=="/padres")
    {
      
      this.tipo="Para padres"
    }
  }
  obtenerElementos()
  {
    
    this.elementService.getElementos().subscribe(result=>{
      result.forEach(e=>
        { 
          
          if(('/'+e.tipo.toLowerCase())==this.router.url)
          {
            let elm= new Element();
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;
            elm.tipo_cultivo=e.tipo_cultivo;
            if(this.carnes.length<NUMERO_CARNES_RECIENTES)
              {this.carnes.push(elm);
                this.carnes.sort(this.ordenar);
              }
          }
        }
       
        );
    });
    
  }
  ordenar(a,b){   
    return a.fecha.seconds-b.fecha.seconds;
  }
ngDoCheck(): void {
 this.tipo=this.capitalize(this.router.url.slice(1));
}
verCarne(c:Element)
  {
    console.log("VER");
    const dialogRef = this.dialog.open(VerCarneComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.carne=c; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
capitalize(word:string) {
  return word[0].toUpperCase() + word.slice(1);
}

}
