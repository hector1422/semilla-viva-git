/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HortalizasComponent } from './hortalizas.component';

describe('HortalizasComponent', () => {
  let component: HortalizasComponent;
  let fixture: ComponentFixture<HortalizasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HortalizasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HortalizasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
