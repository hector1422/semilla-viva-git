import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VerHortalizaComponent } from '../ver-hortaliza/ver-hortaliza.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hortalizas',
  templateUrl: './hortalizas.component.html',
  styleUrls: ['./hortalizas.component.scss']
})
export class HortalizasComponent implements OnInit {
  hortalizas:Element[];
  searchedKeyword: string;
  @Input() tipo:string;
  
  constructor(private elementService:ElementUsuarioService, private dialog: MatDialog,
    private router: Router) 
  {
    this.hortalizas= [];
    this.tipo="Hortalizas";
   }
 
  ngOnInit(): void {
    this.obtenerElementos();
    this.tipo=this.router.url;
    if(this.tipo=="/padres")
    {
      
      this.tipo="Para padres";
    }
  }
  obtenerElementos()
  {
    this.elementService.getElementos().subscribe(result=>{  
      result.forEach(e=>
        {
          if(('/'+e.tipo.toLowerCase())==this.router.url)
          {
            
            let elm= new Element();
            elm.productor=e.productor;
            elm.telefono_productor=e.telefono_productor;
            elm.hora_atencion=e.hora_atencion;
            elm.ubicacion_productor=e.ubicacion_productor;
            elm.tipo=e.tipo;
            elm.imagen=e.imagen;   
            elm.titulo=e.titulo;
            elm.tipo_cultivo=e.tipo_cultivo; 
            elm.resumen=e.resumen;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;      
            this.hortalizas.push(elm);
          }
        }
       
        );
    });
  }
ngDoCheck(): void {
 this.tipo=this.capitalize(this.router.url.slice(1));  
}
  verHortalizas(h:Element)
  {
    const dialogRef = this.dialog.open(VerHortalizaComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.hortaliza=h; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
  capitalize(word:string) {
    return word[0].toUpperCase() + word.slice(1);
  }
}
