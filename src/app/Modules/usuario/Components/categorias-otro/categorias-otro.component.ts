import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { VerOtroComponent } from '../ver-otro/ver-otro.component';
const NUMERO_OTROS_RECIENTES=3;
@Component({
  selector: 'app-categorias-otro',
  templateUrl: './categorias-otro.component.html',
  styleUrls: ['./categorias-otro.component.scss']
})
export class CategoriasOtroComponent implements OnInit {
  panelOpenState = false;
  otros:Element[];
  @Input() tipo:string;
  constructor(private elementService:ElementUsuarioService, private router: Router,
    private dialog: MatDialog) 
  { 
    this.otros= [];
    this.tipo="Otros";
  }

  ngOnInit(): void {
    this.obtenerElementos();
    this.tipo=this.router.url;
    if(this.tipo=="/padres")
    {
      
      this.tipo="Para padres"
    }
  }
  obtenerElementos()
  {
    
    this.elementService.getElementos().subscribe(result=>{
      result.forEach(e=>
        { 
          
          if(('/'+e.tipo.toLowerCase())==this.router.url)
          {
            let elm= new Element();
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;
            elm.tipo_cultivo=e.tipo_cultivo;
            if(this.otros.length<NUMERO_OTROS_RECIENTES)
              {this.otros.push(elm);
                this.otros.sort(this.ordenar);
              }
          }
        }
       
        );
    });
    
  }
  ordenar(a,b){   
    return a.fecha.seconds-b.fecha.seconds;
  }
ngDoCheck(): void {
 this.tipo=this.capitalize(this.router.url.slice(1));
}
verOtro(o:Element)
  {
    console.log("VER");
    const dialogRef = this.dialog.open(VerOtroComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.otro=o; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
capitalize(word:string) {
  return word[0].toUpperCase() + word.slice(1);
}
}
