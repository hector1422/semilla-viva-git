import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ubicacion-usuario',
  templateUrl: './ubicacion-usuario.component.html',
  styleUrls: ['./ubicacion-usuario.component.scss']
})
export class UbicacionUsuarioComponent implements OnInit {
  lat = 2.443048;
  lng = -76.605324;
  zoom = 15;
  constructor() { }

  ngOnInit() {
  }

}
