import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ver-grano',
  templateUrl: './ver-grano.component.html',
  styleUrls: ['./ver-grano.component.scss']
})
export class VerGranoComponent implements OnInit {
  grano:Element;
  constructor(public dialogoReg:MatDialogRef<VerGranoComponent>) 
  { 
    this.grano=new Element();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }
}
