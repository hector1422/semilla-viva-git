/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { VerGranoComponent } from './ver-grano.component';

describe('VerGranoComponent', () => {
  let component: VerGranoComponent;
  let fixture: ComponentFixture<VerGranoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerGranoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerGranoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
