import { Component, OnInit } from '@angular/core';
import { Pediatra } from 'src/app/Model/pediatra.model';
import { PediatrasService } from 'src/app/Services/pediatras.service';
import { MatDialog } from '@angular/material/dialog';
import { VerPediatraUsuarioComponent } from '../ver-pediatra-usuario/ver-pediatra-usuario.component';
@Component({
  selector: 'app-list-pediatras',
  templateUrl: './list-pediatras.component.html',
  styleUrls: ['./list-pediatras.component.scss']
})
export class ListPediatrasComponent implements OnInit {
  pediatras: Pediatra[];
  constructor(private pediatraService: PediatrasService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.pediatras=[];
    this.obtenerElementos();
  }
  obtenerElementos() {
    
    this.pediatraService.getPediatras().subscribe(result => {this.pediatras=[];  
      result.forEach(e => {
        this.pediatras.push(e);
      }
      );      
    });
   
  }
  verPediatra(p: Pediatra) {
    const dialogRef = this.dialog.open(VerPediatraUsuarioComponent, {
      width: '800px',
      height: '500px',
      data: {}
    });
    dialogRef.componentInstance.p = p;
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }

}
