import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ver-semilla',
  templateUrl: './ver-semilla.component.html',
  styleUrls: ['./ver-semilla.component.scss']
})
export class VerSemillaComponent implements OnInit {
  semilla:Element;
  constructor(public dialogoReg:MatDialogRef<VerSemillaComponent>) 
  { 
    this.semilla=new Element();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }

}
