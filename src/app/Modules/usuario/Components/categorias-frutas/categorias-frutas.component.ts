import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { VerFrutaComponent } from '../ver-fruta/ver-fruta.component';
const NUMERO_FRUTAS_RECIENTES=3;

@Component({
  selector: 'app-categorias-frutas',
  templateUrl: './categorias-frutas.component.html',
  styleUrls: ['./categorias-frutas.component.scss']
})
export class CategoriasFrutasComponent implements OnInit {
  panelOpenState = false;
  frutas:Element[];
  @Input() tipo:string;
  constructor(private elementService:ElementUsuarioService, private router: Router,
    private dialog: MatDialog) 
  { 
    this.frutas= [];
    this.tipo="Frutas";
  }

  ngOnInit(): void {
    this.obtenerElementos();
    this.tipo=this.router.url;
  }
  obtenerElementos()
  {
    
    this.elementService.getElementos().subscribe(result=>{
      result.forEach(e=>
        { 
          
          if(('/'+e.tipo.toLowerCase())==this.router.url)
          {
            let elm= new Element();
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;
            elm.tipo_cultivo=e.tipo_cultivo;
            if(this.frutas.length<NUMERO_FRUTAS_RECIENTES)
              {this.frutas.push(elm);
                this.frutas.sort(this.ordenar);
              }
          }
        }
       
        );
    });
    
  }
  ordenar(a,b){   
    return a.fecha.seconds-b.fecha.seconds;
  }
ngDoCheck(): void {
 this.tipo=this.capitalize(this.router.url.slice(1));
}
verFruta(f:Element)
  {
    console.log("VER");
    const dialogRef = this.dialog.open(VerFrutaComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.fruta=f; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
capitalize(word:string) {
  return word[0].toUpperCase() + word.slice(1);
}

}
