import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ver-fruta',
  templateUrl: './ver-fruta.component.html',
  styleUrls: ['./ver-fruta.component.scss']
})
export class VerFrutaComponent implements OnInit {
  fruta:Element;
  constructor(public dialogoReg:MatDialogRef<VerFrutaComponent>) 
  { 
    this.fruta=new Element();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }
}
