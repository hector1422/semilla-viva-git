import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ver-carne',
  templateUrl: './ver-carne.component.html',
  styleUrls: ['./ver-carne.component.scss']
})
export class VerCarneComponent implements OnInit {
  carne:Element;
  constructor(public dialogoReg:MatDialogRef<VerCarneComponent>) 
  { 
    this.carne=new Element();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }

}
