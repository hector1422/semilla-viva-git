/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { VerCarneComponent } from './ver-carne.component';

describe('VerCarneComponent', () => {
  let component: VerCarneComponent;
  let fixture: ComponentFixture<VerCarneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerCarneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerCarneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
