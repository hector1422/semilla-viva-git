/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { VerOtroComponent } from './ver-otro.component';

describe('VerOtroComponent', () => {
  let component: VerOtroComponent;
  let fixture: ComponentFixture<VerOtroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerOtroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerOtroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
