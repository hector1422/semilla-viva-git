import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-ver-otro',
  templateUrl: './ver-otro.component.html',
  styleUrls: ['./ver-otro.component.scss']
})
export class VerOtroComponent implements OnInit {
  otro:Element;
  constructor(public dialogoReg:MatDialogRef<VerOtroComponent>) 
  { 
    this.otro=new Element();
  }

  ngOnInit() {
  }
  ok()
  {
    this.dialogoReg.close();
  }

}
