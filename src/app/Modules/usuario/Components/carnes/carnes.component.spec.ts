/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CarnesComponent } from './carnes.component';

describe('CarnesComponent', () => {
  let component: CarnesComponent;
  let fixture: ComponentFixture<CarnesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarnesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarnesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
