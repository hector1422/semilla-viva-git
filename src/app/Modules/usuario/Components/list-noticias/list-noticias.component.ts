import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VerNoticiaComponent } from '../ver-noticia/ver-noticia.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-noticias',
  templateUrl: './list-noticias.component.html',
  styleUrls: ['./list-noticias.component.scss']
})
export class ListNoticiasComponent implements OnInit {
  noticias:Element[];
  @Input() tipo:string;
  
  constructor(private elementService:ElementUsuarioService, private dialog: MatDialog,
    private router: Router) 
  {
    this.noticias= [];
    this.tipo="Noticias";
   }
 
  ngOnInit(): void {
    this.obtenerElementos();
    this.tipo=this.router.url;
    if(this.tipo=="/padres")
    {
      
      this.tipo="Para padres";
    }
  }
  obtenerElementos()
  {
    this.elementService.getElementos().subscribe(result=>{  
      result.forEach(e=>
        {
          if(('/'+e.tipo.toLowerCase())==this.router.url)
          {
            
            let elm= new Element();
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;
            elm.tipo_cultivo=e.tipo_cultivo;          
            this.noticias.push(elm);
          }
        }
       
        );
    });
  }
ngDoCheck(): void {
 this.tipo=this.capitalize(this.router.url.slice(1));  
}
  verNoticia(n:Element)
  {
    const dialogRef = this.dialog.open(VerNoticiaComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.noticia=n; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
  capitalize(word:string) {
    return word[0].toUpperCase() + word.slice(1);
  }

}
