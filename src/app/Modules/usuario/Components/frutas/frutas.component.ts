import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { VerFrutaComponent } from '../ver-fruta/ver-fruta.component';

@Component({
  selector: 'app-frutas',
  templateUrl: './frutas.component.html',
  styleUrls: ['./frutas.component.scss']
})
export class FrutasComponent implements OnInit {
  
  frutas:Element[];
  searchedKeyword: string;
  @Input() tipo:string;
  
  constructor(private elementService:ElementUsuarioService, private dialog: MatDialog,
    private router: Router) 
  {
    this.frutas= [];
    this.tipo="Frutas";
   }
 
  ngOnInit(): void {
    this.obtenerElementos();
    this.tipo=this.router.url;
  }
  obtenerElementos()
  {
    this.elementService.getElementos().subscribe(result=>{  
      result.forEach(e=>
        {
          if(('/'+e.tipo.toLowerCase())==this.router.url)
          {
            
            let elm= new Element();
            elm.productor=e.productor;
            elm.telefono_productor=e.telefono_productor;
            elm.hora_atencion=e.hora_atencion;
            elm.ubicacion_productor=e.ubicacion_productor;
            elm.tipo=e.tipo;
            elm.imagen=e.imagen;   
            elm.titulo=e.titulo;
            elm.tipo_cultivo=e.tipo_cultivo; 
            elm.resumen=e.resumen;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            this.frutas.push(elm);
          }
        }
       
        );
    });
  }
ngDoCheck(): void {
 this.tipo=this.capitalize(this.router.url.slice(1));  
}
  verFruta(f:Element)
  {
    const dialogRef = this.dialog.open(VerFrutaComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.fruta=f; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
  capitalize(word:string) {
    return word[0].toUpperCase() + word.slice(1);
  }


}
