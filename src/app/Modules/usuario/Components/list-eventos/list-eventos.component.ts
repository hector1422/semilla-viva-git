import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import {ElementUsuarioService} from '../../Services/element-usuario.service'
import { MatDialog } from '@angular/material/dialog';
import { VerNoticiaComponent } from '../ver-noticia/ver-noticia.component';
@Component({
  selector: 'app-list-eventos',
  templateUrl: './list-eventos.component.html',
  styleUrls: ['./list-eventos.component.scss']
})
export class ListEventosComponent implements OnInit {
  eventos:Element[];
  event:Element;
  not:any;
  vacio=true;
  seleccionado=false;
  constructor(private elementService:ElementUsuarioService, private dialog: MatDialog) { 
    this.eventos= [];
    this.event=new Element();
  }

  ngOnInit(): void {
    this.obtenerElementos();
  }
  obtenerElementos()
  {
    this.elementService.getElementos().subscribe(result=>{
      this.not=result;
      result.forEach(e=>
        {
          if(e.tipo=="Eventos")
          {
            
            let elm= new Element();
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;
            if(this.vacio)
            {
              this.event=elm;
              this.elementService.cambiarEvento(elm);
              this.vacio=false;
            }
            
            this.eventos.push(elm);
          }
        }
       
        );
    });
  }
  verNoticia(n:Element)
  {
    const dialogRef = this.dialog.open(VerNoticiaComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.noticia=n; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
}
