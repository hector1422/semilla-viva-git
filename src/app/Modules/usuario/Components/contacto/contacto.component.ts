import { Email } from './../../../../Model/email.model';
import Swal from 'sweetalert2'
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Component, OnInit } from '@angular/core';
import {MapInfoWindow, MapMarker} from '@angular/google-maps';
import { Contacto } from '../../../../Model/contacto.model';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import {EmailService} from '../../Services/email.service'
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {
  formulario:FormGroup;
  lat = 2.443048;
  lng = -76.605324;
  zoom = 15;
   contacto:Contacto;
   band=false;
   emailTo="";
  constructor(private elementService:ElementUsuarioService, private formBuilder: FormBuilder,
    private emailService:EmailService) {
    this.contacto= new Contacto();
    this.getContacto();
  }

  ngOnInit(): void {
    this.crearFormulario();
    this.getContacto();
    
    /* this.agregarContacto(); */
  }
  crearFormulario() {
    this.formulario = this.formBuilder.group(
      {
        nombre: ['', [Validators.required]],
        correo: ['', [Validators.required, Validators.email]],
        asunto: ['', [Validators.required]],
        mensaje: ['', [Validators.required]]
      });
    this.formulario.valueChanges.subscribe(
      value => {/* 
        console.log("Formulario: ",value); */
      }
    );
  }
  clickedMarker()
  {
    
  }
  errores(): boolean 
  {
    return (this.formulario.get('nombre').hasError('required') ||
    this.formulario.get('correo').hasError('required') ||this.formulario.get('correo').hasError('email') ||
      
      this.formulario.get('asunto').hasError('required') ||
      this.formulario.get('mensaje').hasError('required') 
      ) ? true : false;

  }


  getContacto(){
    this.elementService.getContacto()
    .subscribe(result=>{
      result.forEach(c=>
        {
          this.emailTo=c.correo;
          console.log("EMAIL AGREGADO:  ",this.emailTo);
            let cont= new Contacto();
            cont.celular=c.celular;
            cont.correo=c.correo;
            cont.direccion=c.direccion;
            this.contacto=cont;
        }
        );

    },err=>{
      
    })
  }
  onSubmit()
  {    
   
    let email:Email;
    email={
      emailAdress:this.formulario.get('correo').value,
      nombre:this.formulario.get('nombre').value,
      subject:this.formulario.get('asunto').value,
      message:this.formulario.get('mensaje').value,
      to:this.emailTo  
    };
    //console.log("EMAIL FORMADO:  ",email);
    this.emailService.addEmail(email);
    this.okMessage();
  }
  okMessage() {
    Swal.fire({
      icon: 'success',
      title: 'Mensaje Enviado',
      text: 'Mensaje enviado Correctamente. Gracias!',
    });
    this.ngOnInit();
  }
}
