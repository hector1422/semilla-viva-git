import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { VerGranoComponent } from '../ver-grano/ver-grano.component';
const NUMERO_GRANOS_RECIENTES=3;
@Component({
  selector: 'app-categorias-grano',
  templateUrl: './categorias-grano.component.html',
  styleUrls: ['./categorias-grano.component.scss']
})
export class CategoriasGranoComponent implements OnInit {
  panelOpenState = false;
  granos:Element[];
  @Input() tipo:string;
  constructor(private elementService:ElementUsuarioService, private router: Router,
    private dialog: MatDialog) 
  { 
    this.granos= [];
    this.tipo="Granos";
  }

  ngOnInit(): void {
    this.obtenerElementos();
    this.tipo=this.router.url;
  }
  obtenerElementos()
  {
    
    this.elementService.getElementos().subscribe(result=>{
      result.forEach(e=>
        { 
          
          if(('/'+e.tipo.toLowerCase())==this.router.url)
          {
            let elm= new Element();
            elm.productor=e.productor;
            elm.telefono_productor=e.telefono_productor;
            elm.hora_atencion=e.hora_atencion;
            elm.ubicacion_productor=e.ubicacion_productor;
            elm.tipo=e.tipo;
            elm.imagen=e.imagen;   
            elm.titulo=e.titulo;
            elm.tipo_cultivo=e.tipo_cultivo; 
            elm.resumen=e.resumen;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            if(this.granos.length<NUMERO_GRANOS_RECIENTES)
              {this.granos.push(elm);
                this.granos.sort(this.ordenar);
              }
          }
        }
       
        );
    });
    
  }
  ordenar(a,b){   
    return b.fecha.seconds-a.fecha.seconds;
  }
ngDoCheck(): void {
 this.tipo=this.capitalize(this.router.url.slice(1));
}
verGrano(g:Element)
  {
    console.log("VER");
    const dialogRef = this.dialog.open(VerGranoComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.grano=g; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  }
capitalize(word:string) {
  return word[0].toUpperCase() + word.slice(1);
}
}
