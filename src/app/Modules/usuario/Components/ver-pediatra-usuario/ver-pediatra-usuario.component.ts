import { Pediatra } from './../../../../Model/pediatra.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Element } from '../../../../Model/element.model';
import { ElementUsuarioService } from '../../Services/element-usuario.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
@Component({
  selector: 'app-ver-pediatra-usuario',
  templateUrl: './ver-pediatra-usuario.component.html',
  styleUrls: ['./ver-pediatra-usuario.component.scss']
})
export class VerPediatraUsuarioComponent implements OnInit {
  p:Pediatra;
  e:Element[];
  displayedColumns: string[] = ['Titulo', 'Descripción', 'Resumen', 'Cultivo', 'Imagen'];
  dataSource: MatTableDataSource<Element>
  @ViewChild(MatPaginator, { static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true}) sort: MatSort;
  vacio:boolean;
  
  constructor(public dialogoReg:MatDialogRef<VerPediatraUsuarioComponent>, private elementService: ElementUsuarioService) 
  {
    this.p = new Pediatra();
    this.e = [];
    
  }

  ngOnInit() {
    this.vacio=true;
    
    this.obtenerElementos();
    console.log(`Dialog result: ${this.p.cedula}`);
    this.dataSource = new MatTableDataSource(this.e);
    this.dataSource.paginator = this.paginator;
  }

  obtenerElementos()
  {
    this.elementService.getElementos().subscribe(result=>{  
      result.forEach(e=>
          {
            if((''+e.documento.toLowerCase())==this.p.cedula.toString()){
              let elm= new Element();
              elm.productor=e.productor;
              elm.tipo=e.tipo;
              elm.imagen=e.imagen;   
              elm.titulo=e.titulo;
              elm.tipo_cultivo=e.tipo_cultivo; 
              elm.resumen=e.resumen;
              elm.documento=e.documento;
              elm.descripcion_larga=e.descripcion_larga;      
              this.e.push(elm);
  
            }
          }
        );
        this.dataSource = new MatTableDataSource(this.e);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });
    if(this.e.length>0) this.vacio=false;
  }
  ok()
  {
    this.dialogoReg.close();
  }
}
