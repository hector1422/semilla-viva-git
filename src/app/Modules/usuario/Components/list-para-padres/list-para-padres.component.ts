import { VerPediatraUsuarioComponent } from './../ver-pediatra-usuario/ver-pediatra-usuario.component';
import { Element } from '../../../../Model/element.model';
import { Component, OnInit } from '@angular/core';
import { ElementsService } from 'src/app/Services/elements.service';
import { MatDialog } from '@angular/material/dialog';
import { VerNoticiaComponent } from '../ver-noticia/ver-noticia.component';
@Component({
  selector: 'app-list-para-padres',
  templateUrl: './list-para-padres.component.html',
  styleUrls: ['./list-para-padres.component.scss']
})
export class ListParaPadresComponent implements OnInit {

  padres:Element[];
  event:Element;
  not:any;
  vacio=true;
  seleccionado=false;
  constructor(private elementService:ElementsService, private dialog: MatDialog) 
  {
    this.padres= [];
    this.event=new Element();
   }
 
  ngOnInit(): void {
    this.obtenerElementos();
    this.elementService.cambiarEvento(this.padres[0]);
  }
  asignarEvento(evento:Element)
  {
    this.event=evento;
    this.elementService.cambiarEvento(evento);
    this.seleccionado=true;
  }
  verPadre(p:Element)
  {
    
    const dialogRef = this.dialog.open(VerNoticiaComponent, {
      width: '800px',
      height:'500px',
      data:{}
    });
   dialogRef.componentInstance.noticia=p; 
    dialogRef.afterClosed().subscribe(result => {/* 
      console.log(`Dialog result: ${result}`); */
    });
  
  }
  obtenerElementos()
  {
    this.elementService.getElementos().subscribe(result=>{
      this.not=result;
      result.forEach(e=>
        {
          if(e.tipo.toLowerCase()=="padres")
          {
            
            let elm= new Element();
            elm.titulo=e.titulo;
            elm.descripcion_larga=e.descripcion_larga;
            elm.fecha=e.fecha;
            elm.imagen=e.imagen;
            elm.resumen=e.resumen;
            elm.tipo=e.tipo;
            
            if(this.vacio)
            {
              this.event=elm;
              this.elementService.cambiarEvento(elm);
              this.vacio=false;
            }
            
            this.padres.push(elm);
          }
        }
       
        );
    });
  }

}
