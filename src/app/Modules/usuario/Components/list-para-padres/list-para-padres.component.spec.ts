import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListParaPadresComponent } from './list-para-padres.component';

describe('ListParaPadresComponent', () => {
  let component: ListParaPadresComponent;
  let fixture: ComponentFixture<ListParaPadresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListParaPadresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListParaPadresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
